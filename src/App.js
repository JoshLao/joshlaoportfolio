import React from "react";
import Projects from "./Projects.js";
import Header from "./header.js";
import Footer from "./footer.js";
import HomePage from "./HomePage.js";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

function App() {

  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <BrowserRouter basename={basename}>
      <Header />
      <Routes>
        <Route path="/" element={<HomePage className="container"/>} />
        <Route path="/projects" element={<Projects className="container"/>} />
      </Routes>
      <Footer className='container'/>
    </BrowserRouter>
  );
}

export default App;
