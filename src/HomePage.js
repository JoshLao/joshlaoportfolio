import React from 'react';
import { Link } from 'react-router-dom';
import './App.css';
function HomePage() {
  return (
    <div className=' p-3 row w-75 h-75 d-flex justify-content-center homepage'>
      <img className='col rounded-5' style={{ width: '150px', height: '165px', position:'relative', top:'100px' }} src={require('./images/josh_lao.jpg')} alt="Josh Lao" />

      <div className='col m-3'>
        <div className='description'>Hi! I’m Josh, a passionate full-stack software engineer. I have a keen interest in exploring new technologies, innovative approaches, and evolving patterns in order to build high-quality, valuable products for users. The world of software development is ever-evolving and I am deeply fascinated by the endless possibilities it offers. I made the conscious choice to become a software engineer as I find immense satisfaction in transforming mere pages of code into a fully functioning, efficient application!</div>

      </div>
      <Link to='/projects' id='projects'>
          Projects
        </Link>
    </div>
  );
}

export default HomePage;
