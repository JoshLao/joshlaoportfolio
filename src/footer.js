import React from 'react';
import './App.css';

function Footer() {
    return (
        <footer className="footer-container d-flex justify-content-around start-0">
            <a href="https://gitlab.com/JoshLao">
                <img id='gitlab' src={require('./images/gitlab.png')} alt="Gitlab" />
            </a>
            <a href="https://www.linkedin.com/in/josh-lao/">
                <img id='linkedin' src={require('./images/linkedin.png')} alt="LinkedIn" />
            </a>
        </footer>
    );
}

export default Footer;
