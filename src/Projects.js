import React from 'react';
import { Link } from 'react-router-dom';
const orbitron_font = {
    fontFamily: 'Orbitron',
    color: 'black'
}
const description_font = {
    color: '#000',
    fontSize: '14px',
    fontStyle: 'normal',
    fontWeight: 600,
    lineHeight: 'normal',
    letterSpacing: '1px',
}
const Projects = () => {
    const projects = [
        {
            title: 'Task Manager',
            description: 'An Applicaton made to help better organize and manage your projects and tasks.',
            technologies: ['Python', 'Django', 'HTML', 'CSS'],
            image: './images/taskmanager.png',
            link: 'https://gitlab.com/JoshLao/project-alpha-apr'
        },
        {
            title: 'Car Car',
            description: 'Fullstack developement of an application for the management of a car dealership.',
            technologies: ['Docker', 'Python', 'Django', 'JavaScript', 'React'],
            image: './images/carcar.png',
            link: 'https://gitlab.com/JoshLao/project-beta'
        },
        {
            title: 'The Sweet Spot',
            description: 'Fullstack developement of a candy ecomerse application for the buying and selling of candies.',
            technologies: ['Docker', 'Python', 'FastAPI', 'JavaScript', 'React', 'PostgreSQL'],
            image: './images/thesweetspot.jpg',
            link: 'https://gitlab.com/crunchy-bellpeppers/module3-project-gamma'
        }
        // Add more projects here
    ];

    return (
        <div>
            <Link to='/' id='back' style={{width:'50px', letterSpacing:'4px'}} className='text-center fw-bold position-fixed text-decoration-none btn btn-outline-info'>B A C K</Link>
            <h1 style={orbitron_font} className='text-info text-center font-family-code'>My Projects</h1>
            {projects.map((project, index) => (
                <div key={index}>
                    <h2 style={orbitron_font}>{project.title}</h2>
                    <p style={description_font}>{project.description}</p>
                    <ul className='' style={orbitron_font}>
                        {project.technologies.map((technology, index) => (
                            <li key={index}>{technology}</li>
                        ))}
                    </ul>
                    <img className='pimage' src={require(`${project.image}`)} alt={project.title} />
                    <a style={description_font} className='fs-3 text-warning-emphasis text-decoration-none' href={project.link}>GitLab Repo</a>
                </div>
            ))}
        </div>
    );
};
export default Projects;